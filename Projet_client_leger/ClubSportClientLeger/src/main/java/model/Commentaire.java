package model;

public class Commentaire {
	int idCommentaire;
	int idPublication;
	int idUser;
	String comment;
	/**
	 * @param idCommentaire
	 * @param idPublication
	 * @param idUser
	 * @param comment
	 */
	public Commentaire(int idCommentaire, int idPublication, int idUser, String comment) {
		super();
		this.idCommentaire = idCommentaire;
		this.idPublication = idPublication;
		this.idUser = idUser;
		this.comment = comment;
	}
	/**
	 * @return the idCommentaire
	 */
	public int getIdCommentaire() {
		return idCommentaire;
	}
	/**
	 * @param idCommentaire the idCommentaire to set
	 */
	public void setIdCommentaire(int idCommentaire) {
		this.idCommentaire = idCommentaire;
	}
	/**
	 * @return the idPublication
	 */
	public int getIdPublication() {
		return idPublication;
	}
	/**
	 * @param idPublication the idPublication to set
	 */
	public void setIdPublication(int idPublication) {
		this.idPublication = idPublication;
	}
	/**
	 * @return the idUser
	 */
	public int getIdUser() {
		return idUser;
	}
	/**
	 * @param idUser the idUser to set
	 */
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}
