package model;

public class Abonnement {
	
	private int id ;
	private int idSuiveur;
	private int idSuivie;
	/**
	 * @param id
	 * @param idSuiveur
	 * @param idSuivie
	 */
	public Abonnement(int id, int idSuiveur, int idSuivie) {
		super();
		this.id = id;
		this.idSuiveur = idSuiveur;
		this.idSuivie = idSuivie;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the idSuiveur
	 */
	public int getIdSuiveur() {
		return idSuiveur;
	}
	/**
	 * @param idSuiveur the idSuiveur to set
	 */
	public void setIdSuiveur(int idSuiveur) {
		this.idSuiveur = idSuiveur;
	}
	/**
	 * @return the idSuivie
	 */
	public int getIdSuivie() {
		return idSuivie;
	}
	/**
	 * @param idSuivie the idSuivie to set
	 */
	public void setIdSuivie(int idSuivie) {
		this.idSuivie = idSuivie;
	}
	
	

}
