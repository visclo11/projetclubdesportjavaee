package model;
 
public class Publications {
	int idPublications;
	private String titre;
	private String publication;
	private byte[] image;
	private int idUtilisateur;
	
	/**
	 * Constructeur avec tous les champs
	 */
	public Publications(int id ,String titre, String publication, byte[] image, int idUtilisateur) {
		this.idPublications=id;
		this.titre = titre;
		this.publication = publication;
		this.image = image;
		this.idUtilisateur = idUtilisateur;
	}
 
	/**
	 * @return le titre
	 */
	public String getTitre() {
		return titre;
	}
 
	/**
	 * @param titre à définir
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
 
	/**
	 * @return la publication
	 */
	public String getPublication() {
		return publication;
	}
 
	/**
	 * @param publication à définir
	 */
	public void setPublication(String publication) {
		this.publication = publication;
	}
 
	/**
	 * @return l'image
	 */
	public byte[] getImage() {
		return image;
	}
 
	/**
	 * @param image à définir
	 */
	public void setImage(byte[] image) {
		this.image = image;
	}
 
	/**
	 * @return l'identifiant de l'utilisateur
	 */
	public int getIdUtilisateur() {
		return idUtilisateur;
	}
 
	/**
	 * @param idUtilisateur à définir
	 */
	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	/**
	 * @return the idPublications
	 */
	public int getIdPublications() {
		return idPublications;
	}

	/**
	 * @param idPublications the idPublications to set
	 */
	public void setIdPublications(int idPublications) {
		this.idPublications = idPublications;
	}
	
}
 