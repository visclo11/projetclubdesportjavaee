package model;

public class Likes {
	
	private int id ;
	private int idLiker;
	private int idLikeur;
	/**
	 * @param id
	 * @param idLiker
	 * @param idLikeur
	 */
	public Likes(int id, int idLiker, int idLikeur) {
		super();
		this.id = id;
		this.idLiker = idLiker;
		this.idLikeur = idLikeur;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the idLiker
	 */
	public int getIdLiker() {
		return idLiker;
	}
	/**
	 * @param idLiker the idLiker to set
	 */
	public void setIdLiker(int idLiker) {
		this.idLiker = idLiker;
	}
	/**
	 * @return the idLikeur
	 */
	public int getIdLikeur() {
		return idLikeur;
	}
	/**
	 * @param idLikeur the idLikeur to set
	 */
	public void setIdLikeur(int idLikeur) {
		this.idLikeur = idLikeur;
	}
	
	

}
