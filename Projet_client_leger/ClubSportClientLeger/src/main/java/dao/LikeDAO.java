package dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * This class alow to operate sql method on Likes
 * @author ESIGELEC - Equipe 3
 * @version 2.0
 */
 
public class LikeDAO extends ConnexionDao {

	/**
	 * Constructor
	 */
	public LikeDAO() {
		super();
	}

	/**
	 * This method allow to like a publication
	 * @param email
	 * @param idpub
	 * @return int
	 */
	public int liker(String email,int idpub) {
		String userInsertSQL = "INSERT INTO likes (idlikeur,idpub) VALUES (?,?)";
		try (Connection con = DriverManager.getConnection(URL, LOGIN, PASS);
				PreparedStatement userStmt = con.prepareStatement(userInsertSQL)) {

			// Insertion utilisateur
			userStmt.setString(1, email);
			userStmt.setInt(2, idpub);


			int rowsAffected = userStmt.executeUpdate();

			if (rowsAffected > 0) {
				System.out.println("Liké avec succès !");
			} else {
				System.out.println("Erreur lors du like !");
			}

		} catch (SQLException ee) {
			ee.printStackTrace();
		}
		return 0;
	}
	
	/**
	 * This method allow to dislike a publication
	 * @param email
	 * @param idpub
	 * @return int
	 */
	public int deliker(String email, int idpub) {
	    String userDeleteSQL = "DELETE FROM likes WHERE idlikeur = ? AND idpub = ?";
	    int rowsAffected = 0;
	    try (Connection con = DriverManager.getConnection(URL, LOGIN, PASS);
	         PreparedStatement userStmt = con.prepareStatement(userDeleteSQL)) {

	        // Set parameters
	        userStmt.setString(1, email);
	        userStmt.setInt(2, idpub);

	        // Execute update
	        rowsAffected = userStmt.executeUpdate();

	        if (rowsAffected > 0) {
	            System.out.println("Unliker avec succès !");
	        } else {
	            System.out.println("Erreur lors de Unlike !");
	        }

	    } catch (SQLException ee) {
	        ee.printStackTrace();
	    }
	    return rowsAffected;
	}


	/**
	 * This method allow to know if a publication is liked or not
	 * @param email
	 * @param idpub
	 * @return a boolean represented the state of liked
	 */
	public boolean asLike(String email, int idpub) {
		boolean estLike = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			// Connexion à la base de données
			con = DriverManager.getConnection(URL, LOGIN, PASS);

			// Préparation de la requête SQL
			String sql = "SELECT COUNT(*) FROM likes WHERE  idlikeur = ? AND idpub=?";
			ps = con.prepareStatement(sql);
			ps.setString(1, email);
			ps.setInt(2, idpub);

			// Exécution de la requête
			rs = ps.executeQuery();

			// Vérification du résultat
			if (rs.next() && rs.getInt(1) > 0) {
				estLike = true;
			}
		} catch (SQLException ee) {
			// Gestion des exceptions
			ee.printStackTrace();
		} finally {
			// Fermeture des ressources
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (con != null) con.close();
			} catch (SQLException ignore) {
				ignore.printStackTrace();
			}
		}

		return estLike;
	}
	
	
	/**
	 * This method allow to get the number of like
	 * @param idpub
	 * @return int
	 */
	public int nombreLike(int idpub) {
	    int like = 0;
	    Connection con = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;

	    try {
	        // Connexion à la base de données
	        con = DriverManager.getConnection(URL, LOGIN, PASS);

	        // Préparation de la requête SQL
	        String sql = "SELECT COUNT(*) FROM likes WHERE idpub = ?";
	        ps = con.prepareStatement(sql);
	        ps.setInt(1, idpub);

	        // Exécution de la requête
	        rs = ps.executeQuery();

	        // Récupération du résultat
	        if (rs.next()) {
	            like = rs.getInt(1);
	        }
	    } catch (SQLException ee) {
	        // Gestion des exceptions
	        ee.printStackTrace();
	    } finally {
	        // Fermeture des ressources
	        try {
	            if (rs != null) rs.close();
	            if (ps != null) ps.close();
	            if (con != null) con.close();
	        } catch (SQLException ignore) {
	            ignore.printStackTrace();
	        }
	    }

	    return like;
	}

}

