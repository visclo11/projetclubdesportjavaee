package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Commentaire;
import model.Publications;


/**
 * This class alow to operate sql method on Comment
 * @author ESIGELEC - TIC Department
 * @version 2.0
 */
public class CommentDAO extends ConnexionDao {

	/**
	 * Constructor
	 */
	public CommentDAO() {
		super();
	}

/**
 * This method allow to comment a publication
 * @param idPublication
 * @param idUser
 * @param comment
 * @return an int related to the comment
 */
	public int commenter(int idPublication,int idUser,String comment) {
		String userInsertSQL = "INSERT INTO commentaires (idPublication,idUser,Commentaires) VALUES (?,?,?)";
		try (Connection con = DriverManager.getConnection(URL, LOGIN, PASS);
				PreparedStatement userStmt = con.prepareStatement(userInsertSQL)) {

			// Insertion utilisateur
			userStmt.setInt(1, idPublication);
			userStmt.setInt(2, idUser);
			userStmt.setString(3, comment);


			int rowsAffected = userStmt.executeUpdate();

			if (rowsAffected > 0) {
				System.out.println("Utilisateur ajouté avec succès !");
			} else {
				System.out.println("Erreur lors de l'ajout de l'utilisateur !");
			}

		} catch (SQLException ee) {
			ee.printStackTrace();
		}
		return 0;
	}
	
	
/**
 * This method allows to get all the comments
 * @param idpub
 * @return the list of all the comments
 */
	public ArrayList<Commentaire> getCommentaires(int idpub) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Commentaire> returnValue = new ArrayList<Commentaire>();
		try {
			// Connexion à la base de données
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// Requête SQL pour récupérer les codes de coordonnées en fonction de la région et de la fédération
 
			ps = con.prepareStatement("SELECT * FROM commentaires WHERE idPublication=?");
			// Exécution de la requête et traitement des résultats
			ps.setInt(1, idpub);
			rs = ps.executeQuery();
			while (rs.next()) {
				// Création d'un objet CodeCoordonnees à partir des données de la base de données
				returnValue.add(new Commentaire(rs.getInt("idComment"),rs.getInt("idPublication"),rs.getInt("idUser"),rs.getString("Commentaires")));
			}
		} catch (Exception ee) {
			// Gestion des exceptions
			ee.printStackTrace();
		} finally {
			// Fermeture des ressources
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		// Retourne la liste de codes de coordonnées obtenue
		return returnValue;
	}
}
	