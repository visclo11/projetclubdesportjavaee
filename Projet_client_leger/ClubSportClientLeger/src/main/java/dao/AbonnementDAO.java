package dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
  * This class alloows to operate sql method with Abonnement instance
  * @author ESIGELEC - Equipe 3
  * @version 2.0
 */
public class AbonnementDAO extends ConnexionDao {

/**
 * Constructor
 */
	public AbonnementDAO() {
		super();
	}

	/**
	 * This method allow to follow another person
	 * @param email
	 * @param idSuivie
	 * @return 0 if it worked
	 */
	public int abonner(String email,int idSuivie) {
		String userInsertSQL = "INSERT INTO abonnement (idsuiveur,idsuivie) VALUES (?,?)";
		try (Connection con = DriverManager.getConnection(URL, LOGIN, PASS);
				PreparedStatement userStmt = con.prepareStatement(userInsertSQL)) {

			// Insertion utilisateur
			userStmt.setString(1, email);
			userStmt.setInt(2, idSuivie);


			int rowsAffected = userStmt.executeUpdate();

			if (rowsAffected > 0) {
				System.out.println("Utilisateur ajouté avec succès !");
			} else {
				System.out.println("Erreur lors de l'ajout de l'utilisateur !");
			}

		} catch (SQLException ee) {
			ee.printStackTrace();
		}
		return 0;
	}
	
	/**
	 * This method allow to nfollow someone
	 * @param email
	 * @param idSuivie
	 * @return all the affecetd rows
	 */
	public int desabonner(String email, int idSuivie) {
	    String userDeleteSQL = "DELETE FROM abonnement WHERE idsuiveur = ? AND idsuivie = ?";
	    int rowsAffected = 0;
	    try (Connection con = DriverManager.getConnection(URL, LOGIN, PASS);
	         PreparedStatement userStmt = con.prepareStatement(userDeleteSQL)) {

	        // Set parameters
	        userStmt.setString(1, email);
	        userStmt.setInt(2, idSuivie);

	        // Execute update
	        rowsAffected = userStmt.executeUpdate();

	        if (rowsAffected > 0) {
	            System.out.println("Utilisateur supprimé avec succès !");
	        } else {
	            System.out.println("Erreur lors de la suppression de l'utilisateur !");
	        }

	    } catch (SQLException ee) {
	        ee.printStackTrace();
	    }
	    return rowsAffected;
	}


	/**
	 * This allow to verify if someone is following by someone else
	 * @param email
	 * @param idSuivie
	 * @return a boolean related to the state of foolowing
	 */
	public boolean estAbonne(String email, int idSuivie) {
		boolean estAbonne = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			// Connexion à la base de données
			con = DriverManager.getConnection(URL, LOGIN, PASS);

			// Préparation de la requête SQL
			String sql = "SELECT COUNT(*) FROM abonnement WHERE idsuiveur = ? AND idsuivie = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1, email);
			ps.setInt(2, idSuivie);

			// Exécution de la requête
			rs = ps.executeQuery();

			// Vérification du résultat
			if (rs.next() && rs.getInt(1) > 0) {
				estAbonne = true;
			}
		} catch (SQLException ee) {
			// Gestion des exceptions
			ee.printStackTrace();
		} finally {
			// Fermeture des ressources
			try {
				if (rs != null) rs.close();
				if (ps != null) ps.close();
				if (con != null) con.close();
			} catch (SQLException ignore) {
				ignore.printStackTrace();
			}
		}

		return estAbonne;
	}
}

