package dao;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.CodeCoordonnees;
import model.License;

/**This class alow to operate sql method on Licences
 * @author ESIGELEC - Equipe 3
 * @version 2.0
 */
public class LicenseDAO extends ConnexionDao{
	
	/**
	 * Constructor
	 */
	public LicenseDAO() {
        super();
    }
	
	/**
	 * This method allows to get all the licence by departement and region and commune and federation
	 * @param codeDepartement
	 * @param nomCommune
	 * @param nomRegion
	 * @param nomFederation
	 * @return License 
	 */
	public License getLicenseByDepartementAndRegionAndCommuneAndFederation(String codeDepartement,String nomCommune,String nomRegion,String nomFederation) {
    		Connection con = null;
		    PreparedStatement ps = null;
		    ResultSet rs = null;
		    License license = null;

		    try {
		        con = DriverManager.getConnection(URL, LOGIN, PASS);
		        String query = "SELECT * FROM licence WHERE Departement=? AND Commune=? AND Region=? AND Federation=?";
		        ps = con.prepareStatement(query);
		        ps.setString(1, codeDepartement);
		        ps.setString(2, nomCommune);
		        ps.setString(3, nomRegion);
		        ps.setString(4, nomFederation);
		        
		        rs = ps.executeQuery();

		        if (rs.next()) {
		            license = new License(
		                rs.getString("Code_Commune"),
		                rs.getString("Commune"),
		                rs.getString("Departement"),
		                rs.getString("Region"),
		                rs.getString("Code"),
		                rs.getString("Federation"),
		                rs.getInt("F_1_4_ans"),
		                rs.getInt("F_5_9_ans"),
		                rs.getInt("F_10_14_ans"),
		                rs.getInt("F_15_19_ans"),
		                rs.getInt("F_20_24_ans"),
		                rs.getInt("F_25_29_ans"),
		                rs.getInt("F_30_34_ans"),
		                rs.getInt("F_35_39_ans"),
		                rs.getInt("F_40_44_ans"),
		                rs.getInt("F_45_49_ans"),
		                rs.getInt("F_50_54_ans"),
		                rs.getInt("F_55_59_ans"),
		                rs.getInt("F_60_64_ans"),
		                rs.getInt("F_65_69_ans"),
		                rs.getInt("F_70_74_ans"),
		                rs.getInt("F_75_79_ans"),
		                rs.getInt("F_80_99_ans"),
		                rs.getInt("H_1_4_ans"),
		                rs.getInt("H_5_9_ans"),
		                rs.getInt("H_10_14_ans"),
		                rs.getInt("H_15_19_ans"),
		                rs.getInt("H_20_24_ans"),
		                rs.getInt("H_25_29_ans"),
		                rs.getInt("H_30_34_ans"),
		                rs.getInt("H_35_39_ans"),
		                rs.getInt("H_40_44_ans"),
		                rs.getInt("H_45_49_ans"),	                
		                rs.getInt("H_50_54_ans"),
		                rs.getInt("H_55_59_ans"),
		                rs.getInt("H_60_64_ans"),
		                rs.getInt("H_65_69_ans"),
		                rs.getInt("H_70_74_ans"),
		                rs.getInt("H_75_79_ans"),
		                rs.getInt("H_80_99_ans"),
		                rs.getInt("Total")		                
		            );
		        }
		    } catch (SQLException ee) {
		        ee.printStackTrace();
		    } finally {
		        try {
		            if (rs != null) rs.close();
		            if (ps != null) ps.close();
		            if (con != null) con.close();
		        } catch (SQLException ignore) {
		            ignore.printStackTrace();
		        }
		    }
		    return license;
	}
	/**
	 * This mehod allows to get all the code and licens related to the given params
	 * @param codeDepartement
	 * @param nomCommune
	 * @param nomRegion
	 * @param nomFederation
	 * @return ArrayList<CodeCoordonnees>
	 */
	public ArrayList<CodeCoordonnees> getCodeLicenseByDepartementAndRegionAndCommuneAndFederation(String codeDepartement,String nomCommune,String nomRegion,String nomFederation) {
		Connection con = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    ArrayList<CodeCoordonnees> codeCoordonneesList = new ArrayList<>();
 
	    try {
	        con = DriverManager.getConnection(URL, LOGIN, PASS);
	        String query = " SELECT * FROM codecoordonnee WHERE Insee_code IN (SELECT Code_Commune FROM licence WHERE Departement=? AND Commune=? AND Region=? AND Federation=?)";
	        ps = con.prepareStatement(query);
	        ps.setString(1, codeDepartement);
	        ps.setString(2, nomCommune);
	        ps.setString(3, nomRegion);
	        ps.setString(4, nomFederation);
	        System.out.println("LicenseQuery: " + query);
	        System.out.println("Federation: " + nomFederation + ", Departement: " + codeDepartement + ", Region: " + nomRegion + ", Commune: " + nomCommune);
 
	        rs = ps.executeQuery();
 
	        while (rs.next()) {
	        	  CodeCoordonnees code = new CodeCoordonnees(
	                        rs.getString("Insee_code"),
	                        rs.getString("Zip_code"),
	                        rs.getDouble("Latitude"),
	                        rs.getDouble("Longitude"));
	            codeCoordonneesList.add(code);
	        }
	    } catch (SQLException ee) {
	        ee.printStackTrace();
	    } finally {
	        try {
	            if (rs != null) rs.close();
	            if (ps != null) ps.close();
	            if (con != null) con.close();
	        } catch (SQLException ignore) {
	            ignore.printStackTrace();
	        }
	    }
	    return codeCoordonneesList;
}
	
	/**
	 * This method allows to get all licences related to the given params
	 * @param codeDepartement
	 * @param nomCommune
	 * @param nomRegion
	 * @param nomFederation
	 * @return ArrayList<License>
	 */
	 public ArrayList<License> getAllLicensesFromFilters(String codeDepartement, String nomCommune, String nomRegion, String nomFederation) {
	        ArrayList<License> licenses = new ArrayList<>();
	        Connection con = null;
	        PreparedStatement ps = null;
	        ResultSet rs = null;

	        try {
	            con = DriverManager.getConnection(URL, LOGIN, PASS);
	            StringBuilder query = new StringBuilder("SELECT * FROM licence WHERE 1=1");

	            if (codeDepartement != null && !codeDepartement.isEmpty()) {
	                query.append(" AND Departement = ?");
	            }
	            if (nomCommune != null && !nomCommune.isEmpty()) {
	                query.append(" AND Commune = ?");
	            }
	            if (nomRegion != null && !nomRegion.isEmpty()) {
	                query.append(" AND Region = ?");
	            }
	            if (nomFederation != null && !nomFederation.isEmpty()) {
	                query.append(" AND Federation = ?");
	            }

	            ps = con.prepareStatement(query.toString());
	            int index = 1;

	            if (codeDepartement != null && !codeDepartement.isEmpty()) {
	                ps.setString(index++, codeDepartement);
	            }
	            if (nomCommune != null && !nomCommune.isEmpty()) {
	                ps.setString(index++, nomCommune);
	            }
	            if (nomRegion != null && !nomRegion.isEmpty()) {
	                ps.setString(index++, nomRegion);
	            }
	            if (nomFederation != null && !nomFederation.isEmpty()) {
	                ps.setString(index++, nomFederation);
	            }

	            rs = ps.executeQuery();

	            while (rs.next()) {
	                licenses.add(new License(
	                    rs.getString("Code_Commune"),
	                    rs.getString("Commune"),
	                    rs.getString("Departement"),
	                    rs.getString("Region"),
	                    rs.getString("Code"),
	                    rs.getString("Federation"),
	                    rs.getInt("F_1_4_ans"),
	                    rs.getInt("F_5_9_ans"),
	                    rs.getInt("F_10_14_ans"),
	                    rs.getInt("F_15_19_ans"),
	                    rs.getInt("F_20_24_ans"),
	                    rs.getInt("F_25_29_ans"),
	                    rs.getInt("F_30_34_ans"),
	                    rs.getInt("F_35_39_ans"),
	                    rs.getInt("F_40_44_ans"),
	                    rs.getInt("F_45_49_ans"),
	                    rs.getInt("F_50_54_ans"),
	                    rs.getInt("F_55_59_ans"),
	                    rs.getInt("F_60_64_ans"),
	                    rs.getInt("F_65_69_ans"),
	                    rs.getInt("F_70_74_ans"),
	                    rs.getInt("F_75_79_ans"),
	                    rs.getInt("F_80_99_ans"),
	                    rs.getInt("H_1_4_ans"),
	                    rs.getInt("H_5_9_ans"),
	                    rs.getInt("H_10_14_ans"),
	                    rs.getInt("H_15_19_ans"),
	                    rs.getInt("H_20_24_ans"),
	                    rs.getInt("H_25_29_ans"),
	                    rs.getInt("H_30_34_ans"),
	                    rs.getInt("H_35_39_ans"),
	                    rs.getInt("H_40_44_ans"),
	                    rs.getInt("H_45_49_ans"),
	                    rs.getInt("H_50_54_ans"),
	                    rs.getInt("H_55_59_ans"),
	                    rs.getInt("H_60_64_ans"),
	                    rs.getInt("H_65_69_ans"),
	                    rs.getInt("H_70_74_ans"),
	                    rs.getInt("H_75_79_ans"),
	                    rs.getInt("H_80_99_ans"),
	                    rs.getInt("Total")
	                ));
	            }
	        } catch (SQLException ee) {
	            ee.printStackTrace();
	        } finally {
	            try {
	                if (rs != null) rs.close();
	                if (ps != null) ps.close();
	                if (con != null) con.close();
	            } catch (SQLException ignore) {
	                ignore.printStackTrace();
	            }
	        }
	        return licenses;
	    }
	 /**
	  * Pour avoir le nombres de federations en base pour la pagination
	  * @return le nombre de federation
	  */
	 public int countLicencesRelatedToFederation(String nomFederation) {
		    int count = 0;
		    Connection con = null;
		    PreparedStatement ps = null;
		    ResultSet rs = null;

		    try {
		        con = DriverManager.getConnection(URL, LOGIN, PASS);
		        String query = "SELECT COUNT(*) AS total FROM licence WHERE federation = ?";
		        ps = con.prepareStatement(query);
		        ps.setString(1, nomFederation);
		        rs = ps.executeQuery();

		        if (rs.next()) {
		            count = rs.getInt("total");
		        }
		    } catch (SQLException e) {
		        e.printStackTrace();
		    } finally {
		        try {
		            if (rs != null) rs.close();
		            if (ps != null) ps.close();
		            if (con != null) con.close();
		        } catch (SQLException ex) {
		            ex.printStackTrace();
		        }
		    }

		    return count;
		}
	 /**
	  * Permet de récupérer toutes les licences liés à une fédération données
	  * @param nomfederation
	  * @param sortOption
	  * @param page
	  * @param pageSize
	  * @return ArrayList<License> la liste des licences
	  */
	 public ArrayList<License> getAllLicensesFromFederation(String nomfederation,String sortOption,int page, int pageSize) {
		    ArrayList<License> licenses = new ArrayList<>();
		    Connection con = null;
		    PreparedStatement ps = null;
		    ResultSet rs = null;
		    int offset = (page - 1) * pageSize;
		    
		    try {
		        con = DriverManager.getConnection(URL, LOGIN, PASS);
		        String query =("SELECT * FROM licence WHERE federation = ? ORDER BY Total "+sortOption+" LIMIT ? OFFSET ?");
		        ps = con.prepareStatement(query);
		        ps.setString(1, nomfederation);	 
		        ps.setInt(2, pageSize);	       
		        ps.setInt(3, offset);	       

		        rs = ps.executeQuery();

		        while (rs.next()) {
		            licenses.add(new License(
		            		rs.getString("Code_Commune"),
		                    rs.getString("Commune"),
		                    rs.getString("Departement"),
		                    rs.getString("Region"),
		                    rs.getString("Code"),
		                    rs.getString("Federation"),
		                    rs.getInt("F_1_4_ans"),
		                    rs.getInt("F_5_9_ans"),
		                    rs.getInt("F_10_14_ans"),
		                    rs.getInt("F_15_19_ans"),
		                    rs.getInt("F_20_24_ans"),
		                    rs.getInt("F_25_29_ans"),
		                    rs.getInt("F_30_34_ans"),
		                    rs.getInt("F_35_39_ans"),
		                    rs.getInt("F_40_44_ans"),
		                    rs.getInt("F_45_49_ans"),
		                    rs.getInt("F_50_54_ans"),
		                    rs.getInt("F_55_59_ans"),
		                    rs.getInt("F_60_64_ans"),
		                    rs.getInt("F_65_69_ans"),
		                    rs.getInt("F_70_74_ans"),
		                    rs.getInt("F_75_79_ans"),
		                    rs.getInt("F_80_99_ans"),
		                    rs.getInt("H_1_4_ans"),
		                    rs.getInt("H_5_9_ans"),
		                    rs.getInt("H_10_14_ans"),
		                    rs.getInt("H_15_19_ans"),
		                    rs.getInt("H_20_24_ans"),
		                    rs.getInt("H_25_29_ans"),
		                    rs.getInt("H_30_34_ans"),
		                    rs.getInt("H_35_39_ans"),
		                    rs.getInt("H_40_44_ans"),
		                    rs.getInt("H_45_49_ans"),
		                    rs.getInt("H_50_54_ans"),
		                    rs.getInt("H_55_59_ans"),
		                    rs.getInt("H_60_64_ans"),
		                    rs.getInt("H_65_69_ans"),
		                    rs.getInt("H_70_74_ans"),
		                    rs.getInt("H_75_79_ans"),
		                    rs.getInt("H_80_99_ans"),
		                    rs.getInt("Total")
		            ));
		        }
		    } catch (SQLException ee) {
		        ee.printStackTrace();
		    } finally {
		        try {
		            if (rs != null) rs.close();
		            if (ps != null) ps.close();
		            if (con != null) con.close();
		        } catch (SQLException ignore) {
		            ignore.printStackTrace();
		        }
		    }
		    return licenses;
		}
}
