package dao;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
 
import model.CodeCoordonnees;

/**This class alow to operate sql method on Code latitude longitude
 * @author ESIGELEC - TIC Department
 * @version 2.0
 */
// Classe représentant l'accès aux données pour les codes de coordonnées
public class CodeDAO extends ConnexionDao {
    /**
     * Constructor
     */
    public CodeDAO() {
        super();
    }
   
    /**
     * This method allow to get all the club related to the parameters
     * @param nomRegion
     * @param nomFederation
     * @return the list of coordinates
     */
    public ArrayList<CodeCoordonnees> getClubsLites(String nomRegion, String nomFederation) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<CodeCoordonnees> returnValue = new ArrayList<CodeCoordonnees>();
        try {
            // Connexion à la base de données
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            // Requête SQL pour récupérer les codes de coordonnées en fonction de la région et de la fédération
            if (!nomRegion.isEmpty()) {
                ps = con.prepareStatement("SELECT * FROM codecoordonnee WHERE Insee_code IN (SELECT Code_Commune FROM federation WHERE Region = ? AND Federation = ?)");
                ps.setString(1, nomRegion);
                ps.setString(2, nomFederation);
            } else {
                ps = con.prepareStatement("SELECT * FROM codecoordonnee WHERE Insee_code IN (SELECT Code_Commune FROM federation WHERE  Federation = ?)");
                ps.setString(1, nomFederation);
            }
            // Exécution de la requête et traitement des résultats
            rs = ps.executeQuery();
            while (rs.next()) {
                // Création d'un objet CodeCoordonnees à partir des données de la base de données
                returnValue.add(new CodeCoordonnees(rs.getString("Insee_code"), rs.getString("Zip_code"), rs.getDouble("Latitude"), rs.getDouble("Longitude")));
            }
        } catch (Exception ee) {
            // Gestion des exceptions
            ee.printStackTrace();
        } finally {
            // Fermeture des ressources
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception ignore) {
            }
            try {
                if (ps != null)
                    ps.close();
            } catch (Exception ignore) {
            }
            try {
                if (con != null)
                    con.close();
            } catch (Exception ignore) {
            }
        }
        // Retourne la liste de codes de coordonnées obtenue
        return returnValue;
    }

    /**
     * This method allows to get all the clubs relate to the  given params
     * @param nomFederation
     * @return a list of all the clubs
     */
    public ArrayList<CodeCoordonnees> getClubsLitesFederation( String nomFederation) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<CodeCoordonnees> returnValue = new ArrayList<CodeCoordonnees>();
        try {
            // Connexion à la base de données
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            // Requête SQL pour récupérer les codes de coordonnées en fonction de la région et de la fédération
                ps = con.prepareStatement("SELECT * FROM codecoordonnee WHERE Insee_code IN (SELECT Code_Commune FROM federation WHERE  Federation = ?)");
                ps.setString(1, nomFederation);
            // Exécution de la requête et traitement des résultats
            rs = ps.executeQuery();
            while (rs.next()) {
                // Création d'un objet CodeCoordonnees à partir des données de la base de données
                returnValue.add(new CodeCoordonnees(rs.getString("Insee_code"), rs.getString("Zip_code"), rs.getDouble("Latitude"), rs.getDouble("Longitude")));
            }
        } catch (Exception ee) {
            // Gestion des exceptions
            ee.printStackTrace();
        } finally {
            // Fermeture des ressources
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception ignore) {
            }
            try {
                if (ps != null)
                    ps.close();
            } catch (Exception ignore) {
            }
            try {
                if (con != null)
                    con.close();
            } catch (Exception ignore) {
            }
        }
        // Retourne la liste de codes de coordonnées obtenue
        return returnValue;
    }
    
    /**
     * This method allows to get all the clubs relate to the  given params
     * @param CodePostal
     * @param nomFederation
     * @return a list of all the clubs
     */
    public ArrayList<CodeCoordonnees> getClubsLitesByCodePostal(String CodePostal, String nomFederation) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<CodeCoordonnees> returnValue = new ArrayList<CodeCoordonnees>();
        try {
            // Connexion à la base de données
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            // Requête SQL pour récupérer les codes de coordonnées en fonction de la région et de la fédération
            if (!CodePostal.isEmpty()) {
                ps = con.prepareStatement("SELECT * FROM codecoordonnee WHERE Insee_code IN (SELECT Code_Commune FROM federation WHERE Code_Commune IN (SELECT insee FROM commune WHERE codepostal =?)   AND Federation = ?)");
                ps.setString(1, CodePostal);
                ps.setString(2, nomFederation);
            } else {
                ps = con.prepareStatement("SELECT * FROM codecoordonnee WHERE Insee_code IN (SELECT Code_Commune FROM federation WHERE  Federation = ?)");
                ps.setString(1, nomFederation);
            }
            // Exécution de la requête et traitement des résultats
            rs = ps.executeQuery();
            while (rs.next()) {
                // Création d'un objet CodeCoordonnees à partir des données de la base de données
                returnValue.add(new CodeCoordonnees(rs.getString("Insee_code"), rs.getString("Zip_code"), rs.getDouble("Latitude"), rs.getDouble("Longitude")));
            }
        } catch (Exception ee) {
            // Gestion des exceptions
            ee.printStackTrace();
        } finally {
            // Fermeture des ressources
            try {
                if (rs != null)
                    rs.close();
            } catch (Exception ignore) {
            }
            try {
                if (ps != null)
                    ps.close();
            } catch (Exception ignore) {
            }
            try {
                if (con != null)
                    con.close();
            } catch (Exception ignore) {
            }
        }
        // Retourne la liste de codes de coordonnées obtenue
        return returnValue;
    }
}