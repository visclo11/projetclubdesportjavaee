<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
	<link rel="stylesheet" href="css/VerificationCodeForm.css">
	
<title>Code de validation</title>
</head>
<body>
	<jsp:include page="Header.jsp" />
	<div class="section-container">
	<div class="container">
		<div class="card bg-primary text-white">
			<div class="card-body">Veuillez saisir vos identifiants</div>
		</div>
 
		<form action="VerificationCodeController.jsp" method="post">
			 <h2 class="text">Vérification du code </h2>
			<div class="password">
				<input type="text" maxlength="1" name="text1" id="text1" class="input">
                <input type="text" maxlength="1" name="text2" id="text2"class="input">
                <input type="text" maxlength="1" name="text3" id="text3" class="input">
				<input type="text" maxlength="1" name="text4" id="text4" class="input">
			</div>
 
			<button type="submit" class="btn btn-primary custom-btn">Vérifier</button>
		</form>
	</div>
	</div>
<% String message = (String)request.getAttribute("message"); %>
    <% if(message != null && !message.isEmpty()) { %>
        <div class="alert alert-danger">
            <strong>News!</strong> <%= message %>
        </div>
    <% } %>
	<jsp:include page="Footer.jsp" />
</body>
</html>