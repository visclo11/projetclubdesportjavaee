<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.HashSet"%>
<%@ page import="model.Utilisateurs"%>
<%@ page import="dao.UtilisateurDAO"%>
<%@ page import="org.owasp.encoder.Encode"%>



<!DOCTYPE html>
<html lang="en">

<%
Utilisateurs utilisateur = (Utilisateurs) session.getAttribute("utilisateur");
if (utilisateur != null && utilisateur.getRole() == 2) {
%>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Listes des Utilisateurs</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
	integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/ListeUtilisateurs.css">
</head>
<body>

	<jsp:include page="Header.jsp" flush="true" />
	<div class="container">
		
		<%
		String message = (String) request.getAttribute("message");
		%>
		<%
		if (message != null && !message.isEmpty()) {
		%>
		<div class="alert alert-warning alert-dismissible fade show"
			role="alert">
			<strong>Oups!</strong><%=Encode.forHtml(message)%>
			<button type="button" class="btn-close" data-bs-dismiss="alert"
				aria-label="Close"></button>
		</div>
		<%
		}
		%>

		<%
		String param = request.getParameter("cm");
		UtilisateurDAO dao = new UtilisateurDAO();
		ArrayList<Utilisateurs> list = dao.getAllUsers();
		;
		if (list.size() != 0) {
		%>
		<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">Nom</th>
					<th scope="col">Prenom</th>
					<th scope="col">Email</th>
					<th scope="col">Federation</th>
					<th scope="col">Statut</th>
				</tr>
				<%
				for (Utilisateurs user : list) {
				%>
			</thead>


			<tbody>
				<tr>
					<td><%=Encode.forHtml(user.getNom())%></td>
					<td><%=Encode.forHtml(user.getPrenom())%></td>
					<td><%=Encode.forHtml(user.getEmail())%></td>
					<td><%=Encode.forHtml(user.getFederation())%></td>
					<td><%=Encode.forHtml(user.getStatut())%></td>
					<td>
						<div class="dropdown">
							<a class="btn btn-secondary dropdown-toggle" href="#"
								role="button" data-bs-toggle="dropdown" aria-expanded="false">
								Options </a>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item"
									href="ValidateUserController.jsp?nom=<%=Encode.forUriComponent(user.getNom())%>&prenom=<%=Encode.forUriComponent(user.getPrenom())%>&email=<%=Encode.forUriComponent(user.getEmail())%>&motdepasse=<%=Encode.forUriComponent(user.getMotdepasse())%>&federation=<%=Encode.forUriComponent(user.getFederation())%>&club=<%=Encode.forUriComponent(user.getClubs())%>&statut=<%=Encode.forUriComponent(user.getStatut())%>">Valider</a></li>

							</ul>
						</div>
					</td>
				</tr>
			</tbody>
			<%
			}
			%>
		</table>
		<%
		} else {
		%>
		<div class="text-center">
			<h1>Aucune personnes ne s'est inscrit</h1>
		</div>
		<%
		}
		%>
	</div>
</body>
<%
}

else {
response.sendRedirect("Accueil.jsp");
}
%>
</html>
