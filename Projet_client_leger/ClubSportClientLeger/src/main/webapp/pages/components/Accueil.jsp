<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.HashSet"%>
<%@ page import="model.Federation"%>
<%@ page import="dao.FederationDAO"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="org.owasp.encoder.Encode"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Accueil</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
	integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="Header.jsp" />
	
	<% String message = (String)request.getAttribute("message"); %>
	<% if(message != null && !message.isEmpty()) { %>
	<div class="alert alert-warning alert-dismissible fade show"
		role="alert">
		<strong>Ok!</strong><%= Encode.forHtml(message) %>
		<button type="button" class="btn-close" data-bs-dismiss="alert"
			aria-label="Close"></button>
	</div>
	<% } %>
	<div class="sub-part-navbar">
		<h1>LE PORTAIL DES CLUBS ET F�D�RATIONS</h1>
		<p>DECOUVREZ TOUTE L'ACTUALITE DES CLUBS ET F�D�RATIONS PROCHES DE
			CHEZ VOUS</p>
		<div class="sub-part-navbar-bottom-side">
			<p>Affinez votre recherche</p>
		</div>
	</div>

	<form action="Accueil.jsp" method="POST" id="searchForm">
		<div class="search-box">
			<!-- S�lection de f�d�ration -->
			<div class="dropdown">
				<label for="federationSelect">F�d�ration :</label> <select
					id="federationSelect" name="federation" class="form-control">
					<option value="">Toutes les f�d�rations</option>
					<%
                                FederationDAO a = new FederationDAO();
                                List<String> federations = a.getFederations();
                                String selectedFederation = request.getParameter("federation");
                                for (String federation : federations) {
                                    String escapedFederation = Encode.forHtmlAttribute(federation);
                                    String echape = federation.replace("''", "'");
                                %>
					<option value="<%= escapedFederation %>"
						<%= federation.equals(selectedFederation) ? "selected" : "" %>><%= echape %></option>
					<%
                                }
                                %>
				</select>
			</div>

			<%
        // R�cup�ration du terme de recherche
        String federation = request.getParameter("federation") != null ? Encode.forHtmlAttribute(request.getParameter("federation").trim()) : "";
        int currentPage = request.getParameter("page") != null ? Integer.parseInt(request.getParameter("page")) : 1;
        int pageSize = request.getParameter("pageSize") != null ? Integer.parseInt(request.getParameter("pageSize")) : 15;
        %>

			<div class="dropdown">
				<label for="searchTypeSelect">Rechercher par :</label> <select
					id="searchTypeSelect" name="searchType" class="form-control"
					onchange="toggleSearchType()">
					<option value="region"
						<%= "region".equals(request.getParameter("searchType")) ? "selected" : "" %>>R�gion</option>
					<option value="codePostal"
						<%= "codePostal".equals(request.getParameter("searchType")) ? "selected" : "" %>>Code
						Postal</option>
				</select>
			</div>

			<div class="dropdown" id="regionGroup">
				<label for="regionSelect">R�gion :</label> <select id="regionSelect"
					name="region" class="form-control">
					<option value="">Toute la France</option>
					<%
                                 List<String> regions = a.getCommunes();
                                 String selectedRegion = request.getParameter("region");
                                 for (String region : regions) {
                                  String escapedRegion = Encode.forHtmlAttribute(region);
                              %>
					<option value="<%= escapedRegion %>"
						<%= region.equals(selectedRegion) ? "selected" : "" %>><%= escapedRegion %></option>
					<%
                               }
                                %>
				</select>
			</div>

			<div class="dropdown" id="codePostalGroup" style="display: none;">
				<label for="codePostalInput">Code Postal :</label> <input
					type="text" id="codePostalInput" name="codePostal"
					class="form-control">
			</div>

			<div class="submit-group">
				<button type="submit" class="submitButton">Rechercher</button>
			</div>
		</div>
	</form>
	<script>
    function toggleSearchType() {
        var searchType = document.getElementById('searchTypeSelect').value;
        if (searchType === 'region') {
            document.getElementById('regionGroup').style.display = 'block';
            document.getElementById('codePostalGroup').style.display = 'none';
        } else if (searchType === 'codePostal') {
            document.getElementById('regionGroup').style.display = 'none';
            document.getElementById('codePostalGroup').style.display = 'block';
        }
    }

    function searchClubs() {
        var searchType = document.getElementById('searchTypeSelect').value;
        var federation = document.getElementById('federation').value;
        var region = document.getElementById('regionSelect').value;
        var codePostal = document.getElementById('codePostalInput').value;
    }
</script>
	<jsp:include page="FederationTable.jsp" />
	<jsp:include page="Footer.jsp" />
</body>
</html>
