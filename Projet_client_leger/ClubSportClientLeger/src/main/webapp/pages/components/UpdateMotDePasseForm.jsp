<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="org.owasp.encoder.Encode"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
    <link rel="stylesheet" href="css/UpdateMotDePasseForm.css">
    
<title>Changer de mot de passe</title>
 
</head>
<body>
<jsp:include page="Header.jsp" />
<div class="container">
    <div class="card bg-primary text-white">
        <div class="card-body">Changer de mot de passe</div>
    </div>
 
    <form action="UpdateMotDePasseController.jsp" method="post">
        <h2>Nouveau mot de passe</h2>
      
        <div class="form-group">
            <label for="motdepasse1">Nouveau mot de passe</label>
            <input type="password" class="form-control" id="motdepasse1" name="motdepasse1" placeholder="Entrez votre nouveau mot de passe">
        </div>
        <div class="form-group">
            <label for="motdepasse">Confirmez le mot de passe</label>
            <input type="password" class="form-control" id="motdepasse" name="motdepasse" placeholder="Entrez � nouveau votre mot de passe">
        </div>
        
        <button type="submit" class="btn btn-primary custom-btn">Valider</button>
    </form>
</div>
  
<%
    String message = (String)request.getAttribute("message");
%>
<% if(message != null && !message.isEmpty()) { %>
    <div class="alert alert-danger">
        <strong>News!</strong> <%= Encode.forHtml(message) %>
    </div>
<% } %>
<jsp:include page="Footer.jsp" />
</body>
</html>
