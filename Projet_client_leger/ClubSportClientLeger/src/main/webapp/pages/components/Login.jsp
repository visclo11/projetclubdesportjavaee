<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.UtilisateurDAO"%>
<%@ page import="java.security.MessageDigest"%>
<%@ page import="java.security.NoSuchAlgorithmException"%>
<%@ page import="java.util.Base64"%>
<%@ page import="javax.servlet.RequestDispatcher"%>
<%@ page import="javax.servlet.ServletException"%>
<%@ page import="javax.servlet.http.HttpServlet"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="javax.servlet.http.HttpServletResponse"%>
<%@ page import="javax.servlet.http.HttpSession"%>
<%@ page import="model.Utilisateurs"%>
<%@ page import="org.owasp.encoder.Encode"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
</head>
<body>
    <%
        // Récupération des paramètres de la requête
        String email = Encode.forHtml(request.getParameter("email"));
        String motdepasse = Encode.forHtml(request.getParameter("motdepasse"));
 
        // Vérification si les paramètres sont vides
        if (email == null || motdepasse == null || email.isEmpty() || motdepasse.isEmpty()) {
            String message = "Veuillez fournir votre adresse e-mail et votre mot de passe";
            request.setAttribute("message", message);
            session.setAttribute("email", email);
            RequestDispatcher dispatcher = request.getRequestDispatcher("LoginForm.jsp");
            dispatcher.forward(request, response);
        } else {
            // Fonction pour hasher le mot de passe
            String hashedPass = hashPassword(motdepasse);
 
            // Accès à la base de données pour vérifier les informations de connexion
            UtilisateurDAO db = new UtilisateurDAO();
            Utilisateurs utilisateur = db.getUtilisateur(email, hashedPass);
 
            if (utilisateur != null) {
                // Utilisateur trouvé, connexion réussie
                session = request.getSession();
                session.setAttribute("utilisateur", utilisateur);
                session.setAttribute("email", email);
                session.setAttribute("nom", utilisateur.getNom());
                session.setAttribute("id", db.getIdUtilisateur(email, hashedPass));// Sauvegarde de l'idUtilisateur dans la session
                String message = "Vous êtes connecté";
                request.setAttribute("message", message);
        		request.getRequestDispatcher("Accueil.jsp").forward(request, response);
            } else {
                // Adresse e-mail ou mot de passe invalide
                String message = "Adresse e-mail ou mot de passe invalide";
                request.setAttribute("message", message);
                RequestDispatcher dispatcher = request.getRequestDispatcher("LoginForm.jsp");
                dispatcher.forward(request, response);
            }
        }
    %>
 
    <%-- Méthode pour hasher le mot de passe --%>
    <%!
        public String hashPassword(String password) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                byte[] hashedBytes = md.digest(password.getBytes());
                return Base64.getEncoder().encodeToString(hashedBytes);
            } catch (NoSuchAlgorithmException e) {
                // Gérer l'exception NoSuchAlgorithmException
                e.printStackTrace();
                return null;
            }
        }
    %>
</body>
</html>