<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 <%@ page import="org.owasp.encoder.Encode"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous"> 
    
<title>Email form</title>
<link rel="stylesheet" href="css/EmailForm.css">

</head>
<body>
<jsp:include page="Header.jsp" />

<div class="section-container">
<div class="container">
    <div class="card bg-primary text-white">
        <div class="card-body">Veuillez saisir vos identifiants</div>
    </div>
 
    <form action="SendEmailCodeController.jsp" method="post">
        <h2>Saisissez votre email</h2>
      
        <div class="form-group">
            <label for="email">Adresse e-mail</label>
            <input type="text" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Entrez votre adresse e-mail" value="<%= session.getAttribute("email") %>">
        </div>
        
        <button type="submit" class="btn btn-primary custom-btn">Envoyez un code</button>
    </form>
    <% String message = (String)request.getAttribute("sms"); %>
    <% if(message != null && !message.isEmpty()) { %>
        <div class="alert alert-danger">
            <strong>News!</strong> <%= Encode.forHtml(message) %>
        </div>
    <% } %>
</div>
</div>
<jsp:include page="Footer.jsp" />
</body>
</html>
