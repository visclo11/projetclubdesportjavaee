<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="model.Utilisateurs"%>
<%@ page import="dao.UtilisateurDAO"%>
<%@ page import="java.security.MessageDigest"%>
<%@ page import="java.security.NoSuchAlgorithmException"%>
<%@ page import="java.util.Base64"%>
<%@ page import="javax.mail.*" %>
<%@ page import="javax.mail.internet.InternetAddress" %>
<%@ page import="javax.mail.internet.MimeMessage" %>
<%@ page import="java.util.Properties" %>
<%@ page import="org.owasp.encoder.Encode"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
 <%
                Utilisateurs utilisateur = (Utilisateurs) session.getAttribute("utilisateur");
                UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
                if (request.getMethod().equalsIgnoreCase("POST")) {
                    // Récupérer les nouvelles valeurs du formulaire
                    String nom =  Encode.forHtml(request.getParameter("nom"));
                    String prenom = Encode.forHtml( request.getParameter("prenom"));
                    String email =  Encode.forHtml(request.getParameter("email"));
                    String motdepasse =  Encode.forHtml(request.getParameter("motdepasse"));
                    int idUser= utilisateurDAO.getIdUtilisateur(email, utilisateur.getMotdepasse());
                   String hashedPassword=hashPassword(motdepasse);
             
                    // Mettre à jour l'objet utilisateur
                    utilisateur.setNom(nom);
                    utilisateur.setPrenom(prenom);
                    utilisateur.setEmail(email);
                    utilisateur.setMotdepasse(hashedPassword);
               
                    
                    // Mettre à jour l'utilisateur dans la base de données
              
                
                    boolean updateSuccessful = utilisateurDAO.updateUtilisateur(utilisateur,idUser);
 
                    if (updateSuccessful) {
                        session.setAttribute("utilisateur", utilisateur);
                        String message = "Mise à jour réussie!";
    					String messageStatus="good";
                        request.setAttribute("message", message);
    					request.setAttribute("messageStatus", messageStatus);
                        RequestDispatcher dispatcher = request.getRequestDispatcher("Account.jsp");
                        dispatcher.forward(request, response);
                        
                    } else {
                    	 String message = "Échec de la mise à jour. Veuillez réessayer.";
     					String messageStatus="bad";
                         request.setAttribute("message", message);
     					request.setAttribute("messageStatus", messageStatus);
                         RequestDispatcher dispatcher = request.getRequestDispatcher("Account.jsp");
                         dispatcher.forward(request, response);
                    }
                }                  
                %>
                
                <%!
        public String hashPassword(String password1) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                byte[] hashedBytes = md.digest(password1.getBytes());
                return Base64.getEncoder().encodeToString(hashedBytes);
            } catch (NoSuchAlgorithmException e) {
                // Gérer l'exception NoSuchAlgorithmException
                e.printStackTrace();
                return null;
            }
        }
    %>
                
</body>
</html>