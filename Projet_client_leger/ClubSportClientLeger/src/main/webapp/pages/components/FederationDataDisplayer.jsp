<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.License"%>
<%@ page import="model.Federation"%>
<%@ page import="utils.DepartementManager"%>
<%@ page import="dao.LicenseDAO"%>
<%@ page import="dao.FederationDAO"%>
<%@ page import="org.owasp.encoder.Encode"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="./components/componentStyle.css">
</head>
<body>

<table class="dataDisplayer">
    <tbody>
        <%
        String nomFederation = request.getParameter("federation");
        String codeDepartement = request.getParameter("departement");
        String nomRegion = request.getParameter("region");
        String nomCommune = request.getParameter("commune");
        
        DepartementManager manager= new DepartementManager();
        String nomDepartement = manager.getDepartementName(codeDepartement);
        License license = null;
        LicenseDAO licenseDao = new LicenseDAO();
        FederationDAO dao = new FederationDAO();
        Federation federation = null;
        federation = dao.getFederationByDepartementAndRegionAndCommune(nomFederation,codeDepartement,nomRegion,nomCommune);
        license = licenseDao.getLicenseByDepartementAndRegionAndCommuneAndFederation(codeDepartement, nomCommune, nomRegion, nomFederation);
        %>     
        <tr>
            <td><strong>Fédération</strong></td>
            <td><%= nomFederation.replace("''", "'") %></td>
        </tr>
        <tr>
            <td><strong>Département</strong></td>
            <td><%= nomDepartement %> (<%= codeDepartement %>)</td>
        </tr>
        <tr>
            <td><strong>Région</strong></td>
            <td><%= nomRegion %></td>
        </tr>
        <tr>
            <td><strong>Commune</strong></td>
            <td><%= nomCommune.replace("''", "'") %></td>
        </tr>
        <tr>
            <td><strong>Nombre total de licenciés</strong></td>
            <td><%= String.valueOf(license.getTotal()) %></td>
        </tr>
        <tr>
            <td><strong>Nombre total de clubs sportifs</strong></td>
            <td><%= String.valueOf(federation.getClubs()) %></td>
        </tr>
        <tr>
            <td><strong>Nombre total d'Etablissements Professionnels Agréés (EPA)</strong></td>
            <td><%=String.valueOf(federation.getEPA()) %></td>
        </tr>
    </tbody>
</table>
</body>
</html>
