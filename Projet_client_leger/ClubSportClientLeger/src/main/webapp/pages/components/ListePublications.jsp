<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="java.util.ArrayList, model.Publications, dao.PublicationsDAO"%>
<%@ page import="java.util.ArrayList, model.Utilisateurs"%>
<%@ page
	import="java.util.ArrayList, model.Abonnement, dao.AbonnementDAO"%>
	<%@ page
	import="java.util.ArrayList, model.Likes, dao.LikeDAO"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Afficher les publications</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="css/ListePublications.css">
<link rel="stylesheet" href="css/logo.css">
</head>
<body>
	<video autoplay muted loop id="background-video">
		<source src="../Pics/video.mp4" type="video/mp4">
		<!-- Chemin de votre vidéo -->
	</video>

	<jsp:include page="Header.jsp" />

	<!-- Overlay -->
	<div class="container mt-4">
		<div class="row">
			<div id="overlay" class="overlay">
				<div class="overlay-content">
					<form action="#">

						<h1>De qui s'agit-il ?</h1>
						<label for="nom">Nom</label> <input type="text"
							class="form-control" id="nom" name="nom" readonly> <br>
						<br>
						<div class="form-group">
							<label for="nom">Prenom</label> <input type="text"
								class="form-control" id="prenom" name="prenom" readonly>
						</div>
						<br> <br>
						<div class="form-group">
							<label for="nom">Fédération</label> <input type="text"
								class="form-control" id="federation" name="federation" readonly>
						</div>
						<br> <br>
						<div class="form-group">
							<label for="nom">Club</label> <input type="text"
								class="form-control" id="club" name="club" readonly>
						</div>
						<br> <br>
						<div class="form-group">
							<label for="nom">Statut</label> <input type="text"
								class="form-control" id="statut" name="statut" readonly>
						</div>
						<br> <br>

					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container mt-4">
    <div class="row">
        <% 
        // Récupérer les publications depuis la base de données
        PublicationsDAO publicationsDAO = new PublicationsDAO();
        ArrayList<Publications> publications = publicationsDAO.getPublications();

        // Parcourir les publications et les afficher
        for (Publications publication : publications) {
        %>
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div class="card mb-3">
                        <div class="card-header d-flex align-items-center bg-white">
                            <button type="submit" id="avatar-btn" aria-haspopup="true"
                                onclick="toggleOverlay(this)"
                                class="style-scope ytd-topbar-menu-button-renderer btn btn-light"
                                aria-label="Menu du compte"
                                data-id="<%=publication.getIdUtilisateur()%>"
                                data-prenom="<%=publicationsDAO.getUtilisateur(publication.getIdPublications()).getPrenom()%>"
                                data-nom="<%=publicationsDAO.getUtilisateur(publication.getIdPublications()).getNom()%>"
                                data-federation="<%=publicationsDAO.getUtilisateur(publication.getIdPublications()).getFederation()%>"
                                data-club="<%=publicationsDAO.getUtilisateur(publication.getIdPublications()).getClubs()%>"
                                data-statut="<%=publicationsDAO.getUtilisateur(publication.getIdPublications()).getStatut()%>">
                                <yt-img-shadow height="32" width="32"
                                    class="style-scope ytd-topbar-menu-button-renderer no-transition"
                                    loaded="" style="background-color: transparent;">
                                <img id="img" draggable="false"
                                    class="style-scope yt-img-shadow rounded-circle"
                                    alt="Image d'avatar" height="32" width="32"
                                    src="../Pics/imageProfil.png"> </yt-img-shadow>
                            </button>
                            <h5 class="card-title ml-3 mb-0">
                                <%=publicationsDAO.getUtilisateur(publication.getIdPublications()).getNom()%>
                            </h5>

                            <%
                            AbonnementDAO abon = new AbonnementDAO();
                            Utilisateurs utilisateur = (Utilisateurs) session.getAttribute("utilisateur");
                            int idUser = publication.getIdUtilisateur(); 
                            int idpublication = publication.getIdPublications();
                            if (utilisateur != null) {
                            %>

                            <%
                            if (!abon.estAbonne(utilisateur.getEmail(), idUser)) { 
                            %>
                            <button type="button" class="btn btn-primary ml-auto" onclick="location.href='AbonnementController.jsp?idsuivie=<%=idUser%>&statut=sabonner'">Suivre+</button>
                            <%
                            } else { 
                            %>
                            <button type="button" class="btn btn-primary ml-auto" onclick="location.href='AbonnementController.jsp?idsuivie=<%=idUser%>&statut=desabonner'">Suivie</button>
                            <%
                            } 
                            %>

                            <%
                            }
                            %>
                        </div>

                        <div class="card-body">
                            <h5 class="card-title"><%=publication.getTitre()%></h5>
                            <p class="card-text"><%=publication.getPublication()%></p>
                            <img
                                src="data:image/jpeg;base64,<%=org.apache.tomcat.util.codec.binary.Base64.encodeBase64String(publication.getImage())%>"
                                class="card-img-top" alt="<%=publication.getTitre()%>">
                        </div>
                        <!-- Éléments d'interaction en bas -->
                        <div class="card-footer bg-white ">
                            <div class="row">
                                <%
                                LikeDAO like = new LikeDAO();
                              
                                if (utilisateur != null) {
                                    String idLikeur = utilisateur.getEmail(); 
                                    int idpublications = publication.getIdPublications();
                                %>  
                                <% 
                                if (!like.asLike(idLikeur, idpublications)) { 
                                %>
                                <button class="like-button" id="like-button" onclick="location.href='LikeController.jsp?pub=<%=idpublications %>&statut=like'">
                                    <svg class="heart-icon" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 24 24">
                                        <path
                                            d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z" />
                                    </svg>
                                    <%=like.nombreLike(idpublications) %>
                                </button>
                                <% 
                                } else { 
                                %>
                                <button class="like-button" id="like-button" onclick="location.href='LikeController.jsp?pub=<%=idpublications %>&statut=unlike'">
                                    <svg class="heart-icon" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 24 24">
                                        <path
                                            d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z" />
                                    </svg>
                                    <%=like.nombreLike(idpublications) %>
                                </button>
                                <% 
                                } 
                                %>

                                <%
                                }
                                %>
                                <button class="like-buttons" id="like-button"
                                    onclick="toggleOverlays(this)">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M2 4C2 2.89543 2.89543 2 4 2H20C21.1046 2 22 2.89543 22 4V20C22 21.1046 21.1046 22 20 22H4C2.89543 22 2 21.1046 2 20V4Z"
                                            stroke="black" stroke-width="2" />
                                        <path d="M22 4L12 13L2 4"
                                            stroke="black" stroke-width="2" />
                                    </svg>
                                    <a href="Commentaire.jsp?idpub=<%=idpublication%>">Commentaire</a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%
            }
            %>
        </div>
    </div>
</div>


	<script>
		function toggleOverlay(button) {
			var overlay = document.getElementById("overlay");
			var nomInput = document.getElementById("nom");
			var nom = button.getAttribute("data-nom");
			var prenomInput = document.getElementById("prenom");
			var prenom = button.getAttribute("data-prenom");
			var federationInput = document.getElementById("federation");
			var federation = button.getAttribute("data-federation");
			var clubInput = document.getElementById("club");
			var club = button.getAttribute("data-club");
			var statutInput = document.getElementById("statut");
			var statut = button.getAttribute("data-statut");
			var idInput = document.getElementById("id");
			var id = button.getAttribute("data-id");

			if (overlay.style.left === "-100%") {
				overlay.style.display = "block";
				overlay.style.left = "0";
				nomInput.value = nom; // Mettre à jour la valeur de l'input avec le nom
				prenomInput.value = prenom
				federationInput.value = federation
				clubInput.value = club
				statutInput.value = statut
				idInput.value  =id
			} else {
				overlay.style.left = "-100%";
				setTimeout(function() {
					overlay.style.display = "none";
					nomInput.value = ""; // Réinitialiser la valeur de l'input lors de la fermeture de l'overlay
					prenomInput.value = "";
					federationInput.value = "";
					clubInput.value = "";
					statutInput.value = "";
					idInput.value  ="";
				}, 500); // Délai correspondant à la transition
			}
		}
		
		function toggleOverlays(button) {
		      var overlay = document.getElementById("overlays");
		      overlay.classList.toggle("active");
		    }
	</script>
</body>
</html>