<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="org.owasp.encoder.Encode"%> <!-- Import de l'encodeur OWASP -->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><%= Encode.forHtml("Insert title here") %></title> <!-- Encodage du titre -->
<link rel="stylesheet" href="./components/componentStyle.css">
</head>
<body>
    <%
        String buttonLabel = request.getParameter("buttonLabel");
        ArrayList<String> links = (ArrayList<String>) request.getAttribute("links");
    %>
    <div class="dropdown">
        <label for="<%= Encode.forHtmlAttribute(buttonLabel.toLowerCase()) %>"><%= Encode.forHtml(buttonLabel) %></label>
        <select id="<%= Encode.forHtmlAttribute(buttonLabel.toLowerCase()) %>" name="<%= Encode.forHtmlAttribute(buttonLabel.toLowerCase()) %>">
            <option value="">Choisir un <%= Encode.forHtml(buttonLabel) %></option>
            <% 
            if (links != null) {
                for (String link : links) {
                    out.println("<option value='" + Encode.forHtmlAttribute(link) + "'>" + Encode.forHtml(link) + "</option>");
                }
            }
            %>
        </select>
    </div>
</body>
</html>
