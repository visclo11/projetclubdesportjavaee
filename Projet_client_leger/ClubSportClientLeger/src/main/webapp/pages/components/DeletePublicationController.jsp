<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="dao.PublicationsDAO"%>
<%@ page import="org.owasp.encoder.Encode"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
// Récupérer l'ID de la publication à supprimer depuis les paramètres de la requête
    session=request.getSession();
    PublicationsDAO publication =new PublicationsDAO();
    String idPublicationParam = request.getParameter("idPublication");
    String cleanedIdPublication = Encode.forHtml(idPublicationParam);

    
     int idPublication = Integer.parseInt(cleanedIdPublication);
    
    session.setAttribute("idPublication", idPublication);
 
// Appeler la méthode de suppression de la publication dans la base de données
PublicationsDAO publicationsDAO = new PublicationsDAO();
publicationsDAO.deletePublications(idPublication);
 
// Rediriger l'utilisateur vers une page de confirmation ou une autre page appropriée
response.sendRedirect("ListePublicationsUtilisateur.jsp");
%>
</body>
</html>