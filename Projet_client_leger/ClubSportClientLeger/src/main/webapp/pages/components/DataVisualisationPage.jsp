<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.owasp.encoder.Encode" %> <!-- Import de l'encodeur OWASP -->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><%= Encode.forHtml("Insert title here") %></title> <!-- Encodage du titre -->
<link rel="stylesheet" href="style.css">
</head>
<body>
<jsp:include page="Header.jsp" />
<div class="dataVisualisationPage">
    <div class="datadisplayer">
        <jsp:include page="FederationDataDisplayer.jsp" />
        <div class="diagramsdisplayer">
            <div class="licenseRepartitionGraph">
                <jsp:include page="LicenseRepartitionGraph.jsp" />
            </div>
            <div class="statsIndicatorDiagram">
                <jsp:include page="StatsIndicatorDiagram.jsp" />
            </div>
        </div>
    </div>
    <div class="federation-map">
        <jsp:include page="Map2.jsp" />
    </div>
</div> 
<jsp:include page="Footer.jsp" />
</body>
</html>
