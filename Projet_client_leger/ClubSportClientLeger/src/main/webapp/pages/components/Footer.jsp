<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
    <link rel="stylesheet" href="componentStyle.css">
</head>
<body>
<footer class="footer">
  <div>
    <ul>
      <li>Présentation</li>
      <li>Fonctionnalités</li>
      <li>Offres</li>
      <li>Inscription</li>
      <li>Qui sommes-nous ?</li>
    </ul>
  </div>
  <div>
    <ul>
      <li>Portail Sports regions</li>
      <li>Annuaire des clubs</li>
      <li>Accès club abonnés</li>
    </ul>
  </div>
  <div>
    <ul>
      <li>Une question ?</li>
      <li>Consulter notre FAQ</li>
      <li>Questions fréquentes</li>
      <li>Nous contacter</li>
    </ul>
  </div>
</footer>
</body>
</html>