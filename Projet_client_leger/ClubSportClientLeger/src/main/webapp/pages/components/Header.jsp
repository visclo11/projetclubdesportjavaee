<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.Utilisateurs"%>
<%@ page import="java.net.URLEncoder" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="../components/componentStyle.css">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz4fnFO9gybBogGz1HkCw6s9eLrTzIW2H6V7E8R7q5oGFMFXc6DX9eT7E7" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<style>



</style>
</head>
<body>
<nav class="navbar">

    <div class="navbar-links">
        <div class="navbar-link1">
            <%
            Utilisateurs utilisateur = (Utilisateurs) session.getAttribute("utilisateur");
            if (utilisateur != null) {
            %>
                <span>Bienvenue, <%= utilisateur.getNom().toUpperCase() %></span>
            <%
            }
            %>
        </div>
        <div>
            <a href="Accueil.jsp" class="middle-nav-link">Accueil</a>                   
            <a href="Maps.jsp?searchType=" class="middle-nav-link">Map</a>
            <a href="ListePublications.jsp">Actualités</a>
            <%
            if (utilisateur != null && utilisateur.getRole() == 3) {
            %>
                <a href="FederationOnglet.jsp" class="middle-nav-link">Statistiques</a> 
                <a href="ClassementPage.jsp" class="middle-nav-link">Classement</a>  
            <% } %>      
        </div>
        <div class="navbar-link2">
             <% if (utilisateur == null) { %>
              <a href="AddUserForm.jsp">S'inscrire</a>
                <a href="LoginForm.jsp">Se connecter</a>
             <% } else { 
             %>
             <% if (utilisateur.getRole() == 3) { %>
               <a href="PublicationForm.jsp">Publier</a>
               <a href="ListePublicationsUtilisateur.jsp">Administration des Publications</a>
             <% } %>
             <% if (utilisateur.getRole() == 2) { %>            
                <a href="ListeUtilisateurs.jsp">Administration des Utilisateurs</a>
                <a href="AddUserForm1.jsp">Inscription des administrateurs</a>               
             <% } %>   
              <div class="dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Profil
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item popup-text" href="Account.jsp">Editer Profil</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item popup-text" href="LogOut.jsp">Déconnexion</a></li>
                </ul>
            </div>
            <% } %>
            
        </div>
    </div>
</nav>
</body>
</html>
