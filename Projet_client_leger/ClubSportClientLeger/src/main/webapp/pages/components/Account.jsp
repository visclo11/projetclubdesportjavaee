<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="model.Utilisateurs"%>
<%@ page import="model.GravatarUtil"%>
<%@ page import="dao.UtilisateurDAO"%>
<%@ page import="java.util.*" %>
<%@ page import="org.owasp.encoder.Encode"%>

 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Mon Compte</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz4fnFO9gybBogGz1HkCw6s9eLrTzIW2H6V7E8R7q5oGFMFXc6DX9eT7E7" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<link rel="stylesheet" href="css/Account.css">

</head>
<body>
<%
Utilisateurs utilisateur = (Utilisateurs) session.getAttribute("utilisateur");
if (utilisateur != null ) {
%>
    <jsp:include page="Header.jsp" />
    <% String message = (String)request.getAttribute("message");
			String messageStatus = (String)request.getAttribute("messageStatus");			%>
    <% if(message != null && !message.isEmpty()) {
    	if(messageStatus.equals("bad")){
    	%>
        <div class="alert alert-danger">
            <strong>News!</strong> <%= message %>
        </div>
    <%}
    	else if(messageStatus.equals("good")){%>
        <div class="alert alert-success">
            <strong>News!</strong> <%= message %>
        </div>
    
   <%  }
    	} %>
    <%
    String gravatarUrl = GravatarUtil.getGravatarUrl(utilisateur.getEmail());
                if (utilisateur != null) {
                    String roleString = "";
                    switch(utilisateur.getRole()) {
                        case 2:
                            roleString = "Administrateur";
                            break;
                        case 3:
                            roleString = "Acteurs du monde sportif";
                            break;
                        case 4:
                            roleString = "Les �lus";
                            break;
                        default:
                            roleString = "Inconnu";
                            break;
                    }
                    %>
<div class="container">
    <div class="card">
        <div class="card-header">
            Mon Compte
        </div>
        <div class="card-body">
            <div class="user-details">
               
                    <form action="UpdateProfile.jsp" method="post">
                        <div class="user-detail">
                            <label for="nom">Nom :</label>
                            <input type="text" class="form-control" id="nom" name="nom" value="<%= utilisateur.getNom() %>">
                        </div>
                        <div class="user-detail">
                            <label for="prenom">Pr�nom :</label>
                            <input type="text" class="form-control" id="prenom" name="prenom" value="<%= utilisateur.getPrenom() %>">
                        </div>
                        <div class="user-detail">
                            <label for="email">Email :</label>
                            <input type="email" class="form-control" id="email" name="email" value="<%= utilisateur.getEmail() %>">
                        </div>
                        <div class="user-detail">
                            <label for="motdepasse">Mot de passe :</label>
                            <input type="password" class="form-control" id="motdepasse" name="motdepasse" >
                        </div>
                   
                        <button type="submit" class="btn btn-primary">Mettre � jour</button>
                    </form>
                <%
                } else {
                %>
                    <p class="text-danger">Aucun utilisateur connect�.</p>
                <%
                }
                %>
            </div>
            <div class="avatar">
                <img src="<%= gravatarUrl %>" alt="Avatar Utilisateur">
                <p><strong>Avatar</strong></p>
            </div>
        </div>
    </div>
</div>
    <jsp:include page="Footer.jsp" />
 <%
}

else {
response.sendRedirect("Accueil.jsp");
}
%>
</body>
</html>