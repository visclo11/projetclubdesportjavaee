<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList,model.Commentaire, dao.UtilisateurDAO, dao.CommentDAO"%>
<%@ page import="org.owasp.encoder.Encode"%> <!-- Import de l'encodeur OWASP -->

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/commentaires.css">
</head>
<body>
	<jsp:include page="Header.jsp" />
	 <% String message = (String)request.getAttribute("message"); %>
        <% if(message != null && !message.isEmpty()) { %>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
		  <strong>Oups!</strong><%= Encode.forHtml(message) %>
		  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
        <% } %>
	<div class="container">
		<div class="commentaire">
			<div class="messageBox">
				<div class="fileUploadWrapper">
					<label for="file"> <svg xmlns="http://www.w3.org/2000/svg"
							fill="none" viewBox="0 0 337 337">
                                     <circle stroke-width="20"
								stroke="#6c6c6c" fill="none" r="158.5" cy="168.5" cx="168.5"></circle>
                                    <path stroke-linecap="round"
								stroke-width="25" stroke="#6c6c6c" d="M167.759 79V259"></path>
                                    <path stroke-linecap="round"
								stroke-width="25" stroke="#6c6c6c" d="M79 167.138H259"></path>
                                </svg> <span class="tooltip">Add an
							image</span>
					</label> <input type="file" id="file" name="file" />
				</div>
				<form style="display: flex;" action="CommentaireController.jsp" method="post">
				<% String idPublications = request.getParameter("idpub");
				   int idpub=Integer.parseInt(idPublications);
				   %>
				   <input type="hidden" name="idPublication" value="<%= idpub %>">
					<input name="messageInput"placeholder="Message..." type="text"
						id="messageInput" style="flex: 1;" />
					<button id="sendButton" type="submit">
						<svg xmlns="http://www.w3.org/2000/svg" fill="none"
							viewBox="0 0 664 663">
                      <path fill="none"
								d="M646.293 331.888L17.7538 17.6187L155.245 331.888M646.293 331.888L17.753 646.157L155.245 331.888M646.293 331.888L318.735 330.228L155.245 331.888"></path>
                      <path stroke-linejoin="round"
								stroke-linecap="round" stroke-width="33.67" stroke="#6c6c6c"
								d="M646.293 331.888L17.7538 17.6187L155.245 331.888M646.293 331.888L17.753 646.157L155.245 331.888M646.293 331.888L318.735 330.228L155.245 331.888"></path>
                        </svg>
					</button>
				</form>

			</div>
			<br>
			<div class="commentaire">
               <%  CommentDAO dao =new CommentDAO();
                   ArrayList<Commentaire>comment=dao.getCommentaires(idpub);
                   for(int i=0;i<comment.size();i++){
   
                 %>
				<div class="card-header d-flex align-items-center bg-white">
					<button type="submit" id="avatar-btn" aria-haspopup="true"
						class="style-scope ytd-topbar-menu-button-renderer btn btn-light"
						aria-label="Menu du compte">
						<yt-img-shadow height="32" width="32"
							class="style-scope ytd-topbar-menu-button-renderer no-transition"
							loaded="" style="background-color: transparent;">
						<img id="img" draggable="false"
							class="style-scope yt-img-shadow rounded-circle"
							alt="Image d'avatar" height="32" width="32"
							src="../Pics/imageProfil.png"> </yt-img-shadow>
					</button>
					<h5 class="card-title ml-3 mb-0"><%=comment.get(i).getComment() %></h5>
				</div>
				<%} %>
			</div>
			<div class="button">
				<button type="button">
					<a href="ListePublications.jsp">Retour</a>
				</button>
			</div>
		</div>


	</div>

	<jsp:include page="Footer.jsp" />

</body>
</html>