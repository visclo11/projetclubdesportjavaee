<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page  import="dao.AbonnementDAO" %>
<%@ page  import="model.Utilisateurs" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
  
  <%   
  AbonnementDAO abon =new AbonnementDAO();
  Utilisateurs utilisateur =(Utilisateurs) session.getAttribute("utilisateur");
  String statut=request.getParameter("statut");
  String idsuivre =  request.getParameter("idsuivie");
  int idsuivie = Integer.parseInt(idsuivre);
  if (("sabonner").equals(statut)){
	  
	  abon.abonner(utilisateur.getEmail(), idsuivie);
	  response.sendRedirect("ListePublications.jsp");
  }
  
  else{
	  	abon.desabonner(utilisateur.getEmail(), idsuivie);
	    response.sendRedirect("ListePublications.jsp");
  }
  %>
</body>
</html>