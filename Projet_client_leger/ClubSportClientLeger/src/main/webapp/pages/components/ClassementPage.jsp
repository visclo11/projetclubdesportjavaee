<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="model.Federation"%>
<%@ page import="dao.FederationDAO"%>
<%@ page import="dao.LicenseDAO"%>
<%@ page import="model.License"%>
<%@ page import="model.Utilisateurs"%>
<%@ page import="model.FederationUtils"%>
<%@ page import="utils.DepartementManager"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Set"%>
<%@ page import="model.LicenseUtils"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="org.owasp.encoder.Encode" %>
<%

    // Récupération des paramètres du formulaire
    int currentPage = request.getParameter("page") != null ? Integer.parseInt(request.getParameter("page")) : 1;
    int pageSize = request.getParameter("pageSize") != null ? Integer.parseInt(request.getParameter("pageSize")) : 15;
    LicenseDAO dao = new LicenseDAO();
    
    String nomfederation = request.getParameter("federation")!=null? request.getParameter("federation"): "FF de Football";
    String sortOption = request.getParameter("sortOption")!=null? request.getParameter("sortOption") :"DESC";
    
    int totalRecords = dao.countLicencesRelatedToFederation(nomfederation);
    
    ArrayList<License> federationListTab=dao.getAllLicensesFromFederation(nomfederation,sortOption,currentPage,pageSize);
    
    int totalPages = (int) Math.ceil((double) totalRecords / pageSize);
	
    DepartementManager manager=new DepartementManager();
    
    ArrayList<License> federationList = dao.getAllLicensesFromFilters(null,null,null,null);
    ArrayList<String> federations = LicenseUtils.extractFederations(federationList);
%>
<!DOCTYPE html>
<html>
<%
Utilisateurs utilisateur = (Utilisateurs) session.getAttribute("utilisateur");
if (utilisateur != null && utilisateur.getRole() == 3) {
%>
<head>
<meta charset="UTF-8">
<title>Classement</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
  <link rel="stylesheet" href="css/ClassementPage.css">

</head>
<body>

	<jsp:include page="Header.jsp" />

	<div class="sub-part-navbar">
		<h1>VISUALISER LE CLASSEMENT DES DEPARTEMENTS REGIONS ET COMMUNES</h1>
		<p>BASEE SUR LES PROPORTIONS DE LICENSIES DES FEDERATIONS
			SPORTIVES</p>
		<div class="sub-part-navbar-bottom-side">
			<p>Affinez votre recherche</p>
		</div>
	</div>
	<form action="ClassementPage.jsp" method="POST" id="searchForm">
		<div class="search-box">
			<!-- Sélection de fédération -->
			<div class="search-box2">
				<div class="">
					<label for="federationSelect">Fédération :</label> <select
						id="federationSelect" name="federation" class="">
						<option value="">Toutes les fédérations</option>
						<% for (String federation : federations) { %>
						<option value="<%= Encode.forHtml(federation) %>"><%= Encode.forHtml(federation).replace("''", "'") %></option>
						<% } %>
					</select>
				</div>
				<div class="">
					<label for="sortOptionSelect">Option de Tri :</label> <select
						id="sortOptionSelect" name="sortOption">
						<option value="DESC">Décroissant</option>
						<option value="ASC">Croissant</option>
					</select>
				</div>
			</div>
			<!-- Bouton de soumission -->
			<div class="submit-group">
				<label for="distanceSelect">Rechercher les clubs :</label>
				<button type="submit" class="btn btn-primary">Rechercher</button>
			</div>
		</div>
	</form>
	<table class="table-federation-commune">
		<thead>
			<tr>
				<th><strong>Rang</strong></th>
				<th><strong>Département</strong></th>
				<th><strong>Région</strong></th>
				<th><strong>Commune</strong></th>
				<th><strong>Nb Total de licenciés</strong></th>
				<th><strong>Proportion H/F des licences</strong></th>
			</tr>
		</thead>
		<tbody>
			<% int rank = (currentPage - 1) * pageSize + 1; 
    for (License license : federationListTab) { %>
			<tr>
				<td><strong><%= Encode.forHtml(String.valueOf(rank++)) %></strong></td>
				<td><%= Encode.forHtml(manager.getDepartementName(license.getDepartement())) %></td>
				<td><%= Encode.forHtml(license.getRegion().replace("''", "'")) %></td>
				<td><%= Encode.forHtml(license.getCommune().replace("''", "'")) %></td>
				<td><%= Encode.forHtml(String.valueOf(license.getTotal())) %></td>
				<td><%= Encode.forHtml(LicenseUtils.calculateProportionHF(license, license.getTotal())) %></td>
			</tr>
			<% } %>
		</tbody>
	</table>
	<div class="pagination-container">
		<div class="pagination">
			<% int startPage = Math.max(1, currentPage - 5);
        int endPage = Math.min(totalPages, currentPage + 4);
        if (currentPage > 1) { %>
			<a
				href="ClassementPage.jsp?page=<%= Encode.forHtml(String.valueOf(currentPage - 1)) %>&pageSize=<%= Encode.forHtml(String.valueOf(pageSize)) %>&federation=<%= Encode.forHtml(nomfederation) %>&sortOption=<%= Encode.forHtml(sortOption) %>">Précédente</a>
			<% }
        for (int i = startPage; i <= endPage; i++) { %>
			<a
				href="ClassementPage.jsp?page=<%= Encode.forHtml(String.valueOf(i)) %>&pageSize=<%= Encode.forHtml(String.valueOf(pageSize)) %>&federation=<%= Encode.forHtml(nomfederation) %>&sortOption=<%= Encode.forHtml(sortOption) %>"
				<%= i == currentPage ? "class='active'" : "" %>><%= Encode.forHtml(String.valueOf(i)) %></a>
			<% }
        if (currentPage < totalPages) { %>
			<a
				href="ClassementPage.jsp?page=<%= Encode.forHtml(String.valueOf(currentPage + 1)) %>&pageSize=<%= Encode.forHtml(String.valueOf(pageSize)) %>&federation=<%= Encode.forHtml(nomfederation) %>&sortOption=<%= Encode.forHtml(sortOption) %>">Suivante</a>
			<% } %>
		</div>
	</div>
	<jsp:include page="Footer.jsp" />
	 <%
}
else
 {
response.sendRedirect("Accueil.jsp");
}
%>
</body>
</html>