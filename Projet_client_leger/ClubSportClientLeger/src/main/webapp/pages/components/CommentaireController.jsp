<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.Utilisateurs, dao.UtilisateurDAO, dao.CommentDAO"%>
<%@ page import="org.owasp.encoder.Encode"%>

<%
    CommentDAO dao = new CommentDAO();
    UtilisateurDAO dao1 = new UtilisateurDAO();
    
    String commentaire =  Encode.forHtml(request.getParameter("messageInput"));
    
    String idPublication = request.getParameter("idPublication");
    
    int idpub = Integer.parseInt(idPublication);
    
    Utilisateurs utilisateur = (Utilisateurs) session.getAttribute("utilisateur");
    int idUser = 0;
    
    if (utilisateur != null) {
        idUser = dao1.getIdUtilisateur(utilisateur.getEmail(), utilisateur.getMotdepasse());
        dao.commenter(idpub, idUser, commentaire);
        response.sendRedirect("Commentaire.jsp?idpub=" + idPublication);

    } else {
        // Gérer le cas où l'utilisateur n'est pas connecté
        // Par exemple, rediriger vers une page de connexion
        String message = "Vous n'avez pas le droit de commenter cette publication";
                request.setAttribute("message", message);
                RequestDispatcher dispatcher = request.getRequestDispatcher("Commentaire.jsp?idpub=" + idPublication);
                dispatcher.forward(request, response);
        
    }
%>
