<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.owasp.encoder.Encode"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<title>Formulaire de Connexion</title>
<link rel="stylesheet" href="css/LoginForm.css">

</head>
<body>
<jsp:include page="Header.jsp" />
 <% String message = (String)request.getAttribute("message"); %>
        <% if(message != null && !message.isEmpty()) { %>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
		  <strong>Oups!</strong><%= Encode.forHtml(message) %>
		  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
        <% } %>
   <script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
		crossorigin="anonymous"></script>

<div class="big-container">
<div class="container">
    <div class="card bg-primary text-white">
        <div class="card-body">Veuillez saisir vos identifiants</div>
    </div>
 
    <form action="Login.jsp" method="post">
        <h2>Connexion</h2>
      
        <div class="form-group">
            <label for="email">Adresse e-mail</label>
            <input type="text" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Entrez votre adresse e-mail">
        </div>
        <div class="form-group">
            <label for="motdepasse">Mot de passe</label>
            <input type="password" class="form-control" id="motdepasse" name="motdepasse" placeholder="Mot de passe">
        </div>
        <div class="mb-3 form-check">
				<a href="EmailForm.jsp">Mot de passe oublié</a>
			</div>
        <button type="submit" class="btn btn-primary custom-btn">Connexion</button>
    </form>
</div>
 </div>
 
    
<jsp:include page="Footer.jsp" />
</body>
</html>
